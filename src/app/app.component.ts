import { Component, OnInit } from '@angular/core';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';

const RandomQuery = gql`
query RandomQuery {
    randomQuote {
      text
      who
    }
}
`;

interface RandomQueryResponse {
  randomQuote: {
    text: string;
    who: string;
  };
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  loading: boolean;
  text: string;
  who: string;
  constructor(private apollo: Apollo) {

  }

  ngOnInit() {
    this.apollo.watchQuery<RandomQueryResponse>({
      query: RandomQuery,
      pollInterval: 10 * 1000
    }).subscribe(({ data, loading }) => {
      this.loading = loading;
      if (data) {
        this.text = data.randomQuote.text;
        this.who = data.randomQuote.who;
      }
    });
  }
}
