import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ApolloClient, createNetworkInterface } from 'apollo-client';
import { ApolloModule } from 'apollo-angular';

import { AppComponent } from './app.component';

function getParams() {
  const retVal = new Map<string, string>();
  const searchStr = document && document.location && document.location.search && document.location.search.substr(1);
  for (const p of searchStr.split('&')) {
    const eqSign = p.indexOf('=');
    if (eqSign !== -1) {
      retVal.set(decodeURIComponent(p.substr(0, eqSign)).toLowerCase(), decodeURIComponent(p.substr(eqSign + 1)));
    } else {
      retVal.set(decodeURIComponent(p).toLowerCase(), 'true');
    }
  }
  return retVal;
}
const QUERY_ARGS = getParams();
const client = new ApolloClient({
  networkInterface: createNetworkInterface({
    uri: QUERY_ARGS.get('api') || 'http://127.0.0.1/graphql'
  })
});

export function provideClient(): ApolloClient {
  return client;
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    ApolloModule.forRoot(provideClient)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
